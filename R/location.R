##' .. content for \description{} (no empty lines) ..
##'
##' .. content for \details{} ..
##' @title 
##' @return a tibble with location id, name, code and type
##' @author Ahmadou H. Dicko
get_locations <- function() {
  tmp <- jsonlite::fromJSON(fts_GET("/v1/public/location"), simplifyVector = TRUE)
  if (tmp$status != "ok") stop("Something went wrong check your client id and password")
  tibble::as_tibble(tmp$data)
}

## res <- cli$get(path = "/v1/public/location")
## x <- res$parse(encoding = "UTF-8")
## location <- fromJSON(x)
## location <- tibble::as_tibble(location$data)
