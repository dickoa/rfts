fts_organization <- function() {
  res <- cli$get(path = "/v1/public/organization")
  x <- res$parse()
  df <- fromJSON(x)
  parse_org(df)
}