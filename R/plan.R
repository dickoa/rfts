##' .. content for \description{} (no empty lines) ..
##'
##' .. content for \details{} ..
##' @title 
##' @return a tibble with organization id, name
##' @author Ahmadou H. Dicko
get_plans <- function(by = c("code", "id", "year", "iso3"), value = NULL) {
  by <- match.arg(by)
  x <- switch(by,
             code = fts_GET(paste0("/v1/public/plan/", by, "/", value)),
             id = fts_GET(paste0("/v1/public/plan/", by, "/", value)),
             year = fts_GET(paste0("/v1/public/plan/", by, "/", value)),
             iso3 = fts_GET(paste0("/v1/public/plan/country/", value)))
  tmp <- jsonlite::fromJSON(x)
  if (tmp$status != "ok") stop("Something went wrong check your client id and password")
  tibble::as_tibble(tmp$data)
}
