##' .. content for \description{} (no empty lines) ..
##'
##' .. content for \details{} ..
##' @export
##' @title 
##' @return 
##' @author Ahmadou H. Dicko
fts_settings <- function() {
  ops <- list(client_id = Sys.getenv("FTS_CLIENT_ID", ""),
             password = Sys.getenv("FTS_PASSWORD", ""))
  structure(ops, class = "fts_settings")
}


##' @export
print.fts_settings <- function(x, ...) {
  cat("<FTS Settings>", sep = "\n")
  cat("  Base URL: ", fts_base(), "\n")
}

##' .. content for \description{} (no empty lines) ..
##'
##' .. content for \details{} ..
##' @export 
##' @param client_id 
##' @param password
##' @return 
##' @author Ahmadou H. Dicko
fts_setup <- function(client_id = NULL, password = NULL) {
  if (is.null(client_id) | is.null(password)) stop("Need a client id and password to use the FTS API", call. = FALSE)
  Sys.setenv("FTS_CLIENT_ID" = client_id)
  Sys.setenv("FTS_PASSWORD" = password)
}


##' @export
##' @rdname fts_settings
get_url <- function()
  fts_base()

##' @export
##' @rdname fts_settings
get_client_id <- function()
  Sys.getenv("FTS_CLIENT_ID")

##' @export
##' @rdname fts_settings
get_password <- function()
  Sys.getenv("FTS_PASSWORD")
