##' .. content for \description{} (no empty lines) ..
##'
##' .. content for \details{} ..
##' @title 
##' @return a tibble with organization id, name
##' @author Ahmadou H. Dicko
get_projects <- function(by = c("code", "id", "planID", "planCode"), value = NULL) {
  by <- match.arg(by)
  x <- switch(by,
             code = fts_GET(paste0("/v1/public/project/", by, "/", value)),
             id = fts_GET("/v1/public/project/", by, "/", value),
             planID = fts_GET("/v1/public/project/plan", by, "/", value),
             planCode = fts_GET("/v1/public/project/plan", by, "/", value))
  jsonlite::fromJSON(x)
}
