rfts
====

<!-- README.md is generated from README.Rmd. Please edit that file -->
[![GitLab CI Build
Status](https://gitlab.com/dickoa/rfts/badges/master/build.svg)](https://gitlab.com/dickoa/rfts/pipelines)
[![AppVeyror Build
status](https://ci.appveyor.com/api/projects/status/qytbcx7vjq0t9ao5/branch/master?svg=true)](https://ci.appveyor.com/project/dickoa/rfts)
[![Codecov Code
Coverage](https://codecov.io/gl/dickoa/rfts/branch/master/graph/badge.svg)](https://codecov.io/gl/dickoa/rfts)
[![](http://www.r-pkg.org/badges/version/rfts)](http://www.r-pkg.org/pkg/rfts)
[![CRAN RStudio mirror
downloads](http://cranlogs.r-pkg.org/badges/rfts)](http://www.r-pkg.org/pkg/rfts)
[![License:
MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

R Client to OCHA Financial Tracking System (FTS)
------------------------------------------------

An R package to interact with [FTS](https://fts.unocha.org/) data.

Install rfts
------------

You can install the development version from GitLab using
[devtools](https://github.com/hadley/devtools).

    ## install.package("devtools")
    devtools::install_git("https://gitlab.com/dickoa/rfts")

First Usage
-----------

We can query and read conflict data from Mali, it returns a `tibble`

    library(rfts)
    fts_get()

Acknowledgements
----------------
